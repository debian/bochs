Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bochs
Source: https://bochs.sourceforge.io/
Files-Excluded:
 bios/VGABIOS-elpin-*
 bios/BIOS-* bios/VGABIOS-* bios/bios.bin-1.7.5
 bios/i440fx.bin
 bios/acpi-dsdt.hex misc/sb16/sb16ctrl.exe

Files: *
Copyright: MandrakeSoft S.A.
           2001-2025 The Bochs Project
           2002-2025 Stanislav Shwartsman
License: LGPL-2+

Files: bios/acpi-dsdt.dsl
Copyright: 2006 Fabrice Bellard
License: LGPL-2

Files: bios/apmbios.S
Copyright: 2004 Fabrice Bellard
           2005 Struan Bartlett
License: LGPL-2+

Files: bios/rombios32.c
Copyright: 2006      Fabrice Bellard
           2001-2020 The Bochs Project
License: LGPL-2+

Files: bios/rombios32start.S
Copyright: 2006 Fabrice Bellard
License: LGPL-2+

Files:
 bios/rombios.h
 patches/patch.example-user-plugin
 patches/patch.example-vdisk-dll
Copyright: 2006-2018 Volker Ruppert
License: LGPL-2+

Files: build/macosx/diskimage.pl
Copyright: 1991-2002 and beyond by Bungie Studios, Inc.
                                   and the "Aleph One" developers.
License: GPL-2+

Files: bx_debug/bx_parser.cc bx_debug/bx_parser.h
Copyright: 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation, Inc.
License: GPL-3+-Bison
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 As a special exception, you may create a larger work that contains
 part or all of the Bison parser skeleton and distribute that work
 under terms of your choice, so long as that work isn't itself a
 parser generator using the skeleton or a modified version thereof
 as a parser skeleton.  Alternatively, if you modify or redistribute
 the parser skeleton itself, you may (at your option) remove this
 special exception, which will cause the skeleton and the resulting
 Bison output files to be licensed under the GNU General Public
 License without this special exception.

Files:
 cpu/fpu/f2xm1.cc
 cpu/fpu/fpatan.cc
 cpu/fpu/fprem.cc
 cpu/fpu/fpu_constant.h
 cpu/fpu/fsincos.cc
 cpu/fpu/fyl2x.cc
 cpu/fpu/poly.cc
 cpu/fpu/softfloat-specialize.h
Copyright: 1996, 1997, 1998, 2002 John R. Hauser
License: SoftFloat
 SoftFloat was written by John R. Hauser.  Release 2c of SoftFloat was made
 possible in part by the International Computer Science Institute, located
 at Suite 600, 1947 Center Street, Berkeley, California 94704.  Funding
 was partially provided by the National Science Foundation under grant
 MIP-9311980.  The original version of this code was written as part of a
 project to build a fixed-point vector processor in collaboration with the
 University of California at Berkeley, overseen by Profs. Nelson Morgan and
 John Wawrzynek.
 .
 THIS SOFTWARE IS DISTRIBUTED AS IS, FOR FREE.  Although reasonable effort
 has been made to avoid it, THIS SOFTWARE MAY CONTAIN FAULTS THAT WILL AT
 TIMES RESULT IN INCORRECT BEHAVIOR.  USE OF THIS SOFTWARE IS RESTRICTED TO
 PERSONS AND ORGANIZATIONS WHO CAN AND WILL TOLERATE ALL LOSSES, COSTS, OR
 OTHER PROBLEMS THEY INCUR DUE TO THE SOFTWARE WITHOUT RECOMPENSE FROM JOHN
 HAUSER OR THE INTERNATIONAL COMPUTER SCIENCE INSTITUTE, AND WHO FURTHERMORE
 EFFECTIVELY INDEMNIFY JOHN HAUSER AND THE INTERNATIONAL COMPUTER SCIENCE
 INSTITUTE (possibly via similar legal notice) AGAINST ALL LOSSES, COSTS, OR
 OTHER PROBLEMS INCURRED BY THEIR CUSTOMERS AND CLIENTS DUE TO THE SOFTWARE,
 OR INCURRED BY ANYONE DUE TO A DERIVATIVE WORK THEY CREATE USING ANY PART OF
 THE SOFTWARE.
 .
 The following are expressly permitted, even for commercial purposes:
 (1) distribution of SoftFloat in whole or in part, as long as this and
 other legal notices remain and are prominent, and provided also that, for a
 partial distribution, prominent notice is given that it is a subset of the
 original; and
 (2) inclusion or use of SoftFloat in whole or in part in a derivative
 work, provided that the use restrictions above are met and the minimal
 documentation requirements stated in the source code are satisfied.

Files: cpu/apic.*
Copyright: 2002-2019 Zwane Mwaikambo, Stanislav Shwartsman
License: LGPL-2+

Files: cpu/softfloat3e/*
Copyright: 2011-2018 The Regents of the University of California
License: BSD-3-clause-UCB

Files:
 extplugin.h
 plugin.*
Copyright: 1999-2000 The plex86 developers team
           2002-2021 The Bochs Project
License: LGPL-2+ and Expat

Files:
 gui/enh_dbg.*
 gui/gtk_enh_dbg_osdep.cc
 gui/wenhdbg_res.h
 gui/win32_enh_dbg_osdep.cc
Copyright: 2008      Chourdakis Michael
           2008-2021 The Bochs Project
License: LGPL-2+
Comment:
 The source files don’t include a license statement, but
 https://forum.osdev.org/viewtopic.php?f=2&t=17546&start=60 has the
 original author’s license grant: “I'd be happy if my code is part of
 the bochs source from now on, no problem for LGPL.”

Files:
 gui/rfb.cc
 gui/vncsrv.cc
Copyright: 2000      Psyon.Org!
           2001-2021 The Bochs Project
License: LGPL-2+

Files:
 gui/scrollwin.*
 gui/win32paramdlg.*
Copyright: 2009-2021 Volker Ruppert
License: LGPL-2+

Files:
 host/linux/pcidev/kernel_pcidev.h
 host/linux/pcidev/pcidev.c
Copyright: 2003-2004 Frank Cornelis
License: GPL-2

Files:
 iodev/devices.cc
 iodev/iodev.h
 iodev/pcidev.*
 memory/memory-bochs.h
 memory/misc_mem.cc
Copyright: 2003      Frank Cornelis
           2001-2021 The Bochs Project
License: LGPL-2+

Files: iodev/display/svga_cirrus.*
Copyright: 2004      Makoto Suzuki
                     Volker Ruppert
                     Robin Kay
           2004-2021 The Bochs Project
License: LGPL-2+

Files: iodev/display/vga.*
Copyright: 2002-2003 Mike Nordell
           2002-2021 The Bochs Project
License: LGPL-2+

Files:
 iodev/display/banshee.cc
 iodev/display/voodoo_data.h
 iodev/display/voodoo_func.h
 iodev/display/voodoo_main.h
Copyright: Aaron Giles
License: BSD-3-clause-Giles
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.
 * Neither the name 'MAME' nor the names of its contributors may be
   used to endorse or promote products derived from this software
   without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY AARON GILES ''AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL AARON GILES BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Files: iodev/hdimage/vbox.*
Copyright: 2015      Benjamin D Lunt
           2006-2021 The Bochs Project
License: LGPL-2.1+

Files: iodev/hdimage/vmware3.*
Copyright: 2003      Net Integration Technologies, Inc.
           2003-2021 The Bochs Project
License: LGPL-2.1+

Files: iodev/hdimage/vmware4.*
Copyright: 2006      Sharvil Nanavati
           2006-2021 The Bochs Project
License: LGPL-2.1+

Files: iodev/hdimage/vpc.*
Copyright: 2005      Alex Beregszaszi
           2009      Kevin Wolf
           2012-2021 The Bochs Project
License: Expat

Files: iodev/hdimage/vvfat.*
Copyright: 2004-2005 Johannes E. Schindelin
           2010-2021 The Bochs Project
License: Expat

Files: iodev/hpet.*
Copyright: 2007      Alexander Graf
           2008      IBM Corporation
           2017-2019 The Bochs Project
License: LGPL-2+

Files: iodev/network/e1000.*
Copyright: 2004      Antony T Curtis
           2007      Dan Aloni
           2008      Qumranet
           2011-2018 The Bochs Project
License: LGPL-2+

Files:
 iodev/network/eth_socket.cc
 misc/bxhub.cc
Copyright: 2003 Mariusz Matuszek
           2017 The Bochs Project
License: LGPL-2+

Files: iodev/network/eth_vde.cc
Copyright: 2003      Renzo Davoli
           2003-2017 The Bochs Project
License: LGPL-2+

Files: iodev/network/pcipnic.*
Copyright: 2003      Fen Systems Ltd.
           2003-2018 The Bochs Project
License: LGPL-2+

Files: iodev/network/slirp/*
Copyright: 1995-1996 Danny Gasparovski
License: BSD-2-clause-Gasparovski
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 DANNY GASPAROVSKI OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: iodev/network/slirp/arp_table.cc
Copyright: 2011 AdaCore
License: Expat

Files:
 iodev/network/slirp/bootp.cc
 iodev/network/slirp/slirp.cc
Copyright: 2004-2008 Fabrice Bellard
License: Expat

Files:
 iodev/network/slirp/cksum.cc
 iodev/network/slirp/ip.h
 iodev/network/slirp/ip_icmp.*
 iodev/network/slirp/ip_input.cc
 iodev/network/slirp/ip_output.cc
 iodev/network/slirp/mbuf.h
 iodev/network/slirp/tcp.h
 iodev/network/slirp/tcp_input.cc
 iodev/network/slirp/tcpip.h
 iodev/network/slirp/tcp_output.cc
 iodev/network/slirp/tcp_subr.cc
 iodev/network/slirp/tcp_timer.*
 iodev/network/slirp/tcp_var.h
 iodev/network/slirp/udp.*
 qemu-queue.h
Copyright: 1988, 1992, 1993 The Regents of the University of
                            California
License: BSD-3-clause-UCB

Files:
 iodev/network/slirp/compat.*
 iodev/usb/usb_hub.*
Copyright: 2003-2008 Fabrice Bellard
           2009-2023 The Bochs Project
License: Expat

Files: iodev/network/slirp/dnssearch.cc
Copyright: 2012 Klaus Stengel
License: Expat

Files: iodev/network/slirp/tftp.cc
Copyright: 2004 Magnus Damm
License: Expat

Files: iodev/sound/es1370.*
Copyright: 2005      Vassili Karpov
           2011-2018 The Bochs Project
License: Expat

Files: iodev/sound/opl.*
Copyright: 1998-2001 Ken Silverman
           2002-2013 The DOSBox Team
           2015      The Bochs Project
License: LGPL-2.1+

Files: iodev/speaker.*
Copyright: 2003      David N. Welton
           2003-2018 The Bochs Project
License: LGPL-2+

Files:
 iodev/usb/ohci_core.*
 iodev/usb/uhci_core.*
 iodev/usb/usb_floppy.*
 iodev/usb/usb_ohci.*
 iodev/usb/usb_pcap.cc
 iodev/usb/usb_printer.*
 iodev/usb/usb_uasp.cc
 iodev/usb/usb_uhci.*
 iodev/usb/usb_xhci.*
Copyright: 2004-2025 Benjamin D Lunt
           2009-2025 The Bochs Project
License: LGPL-2+

Files:
 iodev/usb/scsi_device.*
 iodev/usb/usb_msd.*
Copyright: 2006      CodeSourcery
           2007-2023 The Bochs Project
License: LGPL-2+

Files:
 iodev/usb/usb_common.*
Copyright: 2005      Fabrice Bellard
           2009-2023 Benjamin D Lunt
           2009-2023 The Bochs Project
License: Expat

Files: iodev/usb/usb_ehci.*
Copyright: 2008      Emutex Ltd.
           2011-2012 Red Hat, Inc.
           2015-2025 The Bochs Project
License: LGPL-2+

Files: iodev/usb/usb_hid.*
Copyright: 2005      Fabrice Bellard
           2007      OpenMoko, Inc.
           2004-2023 Benjamin D Lunt
           2009-2023 The Bochs Project
License: Expat

Files: misc/bxcompat.h
Copyright: 2013      Volker Ruppert
           2001-2017 The Bochs Project
License: LGPL-2+

Files: misc/bximage.cc
Copyright: 2004      Fabrice Bellard
           2005      Filip Nevara
           2005      Alex Beregszaszi
           2009      Kevin Wolf
           2013-2018 Volker Ruppert
           2001-2013 The Bochs Project
License: LGPL-2+ and Expat

Files: misc/spoolpipe.c
Copyright: 2002 Cegis Enterprises, Inc.
License: GPL-2+

Files: debian/*
Copyright: 2002-2004 Robert Millan
           2004-2013 Guillem Jover
           2014      Olly Betts
           2015      Santiago Vila
           2018-2022, 2024-2025 Stephen Kitt
License: LGPL-2.1+

License: BSD-3-clause-UCB
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2 as
 published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in '/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 This license is contained in the file "COPYING",
 which is included with this source code; it is available online at
 http://www.gnu.org/licenses/gpl.html
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in '/usr/share/common-licenses/GPL-2'.

License: LGPL-2
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License version 2 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License, version 2, can be found in
 '/usr/share/common-licenses/LGPL-2'.

License: LGPL-2+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License, version 2, can be found in
 '/usr/share/common-licenses/LGPL-2'.

License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License, version 2.1, can be found in
 '/usr/share/common-licenses/LGPL-2.1'.
