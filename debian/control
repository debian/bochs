Source: bochs
Section: otherosfs
Priority: optional
Maintainer: Stephen Kitt <skitt@debian.org>
Build-Depends:
 bison,
 debhelper-compat (= 13),
 flex,
 libaa1-dev,
 libasound2-dev [linux-any],
 libgtk-3-dev,
 libice-dev,
 libltdl-dev,
 libncurses-dev,
 libpulse-dev,
 libreadline-dev,
 libsdl2-dev,
 libsm-dev,
 libtool,
 libwxgtk3.2-dev,
 libx11-dev,
 libxpm-dev,
 libz-dev
Build-Depends-Indep:
 acpica-tools,
 bcc,
 bin86,
 docbook-dsssl,
 gcc-i686-linux-gnu
Homepage: https://bochs.sourceforge.io/
Vcs-Browser: https://salsa.debian.org/debian/bochs
Vcs-Git: https://salsa.debian.org/debian/bochs.git
Standards-Version: 4.7.2
Rules-Requires-Root: no

Package: bochs
Architecture: any
Depends:
 bochs-wx | bochs-gui,
 bochsbios (= ${source:Version}) | seabios,
 vgabios (>= 0.8~) | seabios,
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 bximage
Suggests:
 bochs-doc,
 debootstrap,
 gcc | c-compiler,
 grub-rescue-pc,
 libc-dev
Description: IA-32 PC emulator
 Bochs is a highly portable free IA-32 (x86) PC emulator written in C++, that
 runs on most popular platforms. It includes emulation of the Intel x86 CPU,
 common I/O devices, and a custom BIOS.
 .
 Bochs is capable of running most operating systems inside the emulation
 including GNU, GNU/Linux, *BSD, FreeDOS, MSDOS and Windows 95/NT.

Package: bochs-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends}
Description: Bochs upstream documentation
 This package contains the HTML documentation of the Bochs project.
 .
 The documentation is divided into three parts:
 .
  * User Guide
  * Development Guide
  * Documentation Guide

Package: bochsbios
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends}
Description: BIOS for the Bochs emulator
 Bochs is a highly portable free IA-32 (x86) PC emulator written in C++, that
 runs on most popular platforms. It includes emulation of the Intel x86 CPU,
 common I/O devices, and a custom BIOS.
 .
 This package contains the BIOS of the Bochs project.

Package: bochs-wx
Architecture: any
Depends:
 bochs (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 bochs-gui
Description: WxWindows plugin for Bochs
 Bochs is a highly portable free IA-32 (x86) PC emulator written in C++, that
 runs on most popular platforms. It includes emulation of the Intel x86 CPU,
 common I/O devices, and a custom BIOS.
 .
 This package contains a WxWindows GUI plugin for Bochs.

Package: bochs-sdl
Architecture: any
Depends:
 bochs (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 bochs-gui
Description: SDL plugin for Bochs
 Bochs is a highly portable free IA-32 (x86) PC emulator written in C++, that
 runs on most popular platforms. It includes emulation of the Intel x86 CPU,
 common I/O devices, and a custom BIOS.
 .
 This package contains an SDL GUI plugin for Bochs.
 .
 Use Scroll-Lock key for full screen.

Package: bochs-term
Architecture: any
Depends:
 bochs (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 bochs-gui
Description: Terminal (ncurses-based) plugin for Bochs
 Bochs is a highly portable free IA-32 (x86) PC emulator written in C++, that
 runs on most popular platforms. It includes emulation of the Intel x86 CPU,
 common I/O devices, and a custom BIOS.
 .
 This package contains a Terminal (ncurses-based) GUI plugin for Bochs.

Package: bochs-x
Architecture: any
Depends:
 bochs (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 bochs-gui
Description: X11 plugin for Bochs
 Bochs is a highly portable free IA-32 (x86) PC emulator written in C++, that
 runs on most popular platforms. It includes emulation of the Intel x86 CPU,
 common I/O devices, and a custom BIOS.
 .
 This package contains an X11 GUI plugin for Bochs.

Package: bximage
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: Disk Image Creation Tool for Bochs
 This tool is part of the Bochs project. Its purpose is to generate
 disk images that are used to allocate the guest operating system in
 Bochs environment.

Package: sb16ctrl-bochs
Architecture: any-i386
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: control utility for Bochs emulated SB16 card
 You can use this utility to perform some query operations on
 the Bochs emulated SB16 card. It needs to be installed in your
 Debian-based guest OS.
 .
 The sb16ctrl utility contained in this package can only be used
 from inside the Bochs emulator. DO NOT TRY IT ON REAL HARDWARE.
